
// API

const baseUrl = 'https://computer-api-production.up.railway.app/'
const apiUrl = "https://computer-api-production.up.railway.app/computers"


// Display items query 

const itemTitleContainer = document.querySelector("#myItem")
const imageContainer = document.querySelector(".card-img-top")
const itemPriceContainer = document.querySelector(".itemPrice")
const itemNameContainer = document.querySelector(".itemName")
const stockAvailableContainer = document.querySelector(".stockAvailableContainer")
const specsContainer = document.querySelector(".specsContainer")
const getLoanBtn = document.querySelector("#getThisItemBtn")


// Get ID for button 

const workBtn = document.getElementById("workBtn")
const transferToBank = document.getElementById("transferToBank")
const payBtn = document.getElementById("payBtn")
const buyThisItemBtn = document.getElementById("buyThisItemBtn")
const itemsOptions = document.getElementById("sel1")
let buyNowButton = document.getElementById("buyThisItemBtn")

// Display value to DOM
const totalWorkAmount = document.getElementById("totalWorkAmount")
const currentBalanceBank = document.getElementById("currentBalance")
const loanStatusDiv = document.getElementById("loanStatusDiv")
const loanAmountContainer = document.getElementById("loanAmountContainer")


// Global variables

let itemImage = ''
let itemTitle = ''
let itemDescription = ''
let itemPrice = ''
let maxLoan = 0
let stockAvailable = ''

// async function in fetching API

async function fetchItemsFromAPI() {
   
    try {
        let response = await fetch(apiUrl)
        if (response.status === 200) {
            data = await response.json()
            return data
        }
    } catch (error) {
        console.log(error)
    }
}

let data = fetchItemsFromAPI()
console.log(data)


async function renderItems(){
    let items = await fetchItemsFromAPI()
    let optionItems = '<option value="0">Select Item</option>'
    console.log(items)
    items.forEach( item => {
        let optionsSegment = '<option class="itemOptions" value="'+item.id+'">'+item.title+'</option>'
        optionItems += optionsSegment
        console.log(optionItems)
    })

    let selectContainer = document.querySelector('#sel1')
    console.log(selectContainer)
    selectContainer.innerHTML = optionItems
}
renderItems()



itemsOptions.addEventListener('change', function(e) {
    console.log(e)
    let optionIdd = e.target.value
    
    if (optionIdd != '0') {
    getItemDetails(parseInt(optionIdd))
    buyNowButton.style.display= "block" 
    console.log( optionIdd )

    }
    
}, false)


// Get Item Details

function getItemDetails(ids){
    
    const arr2 = data.filter(d => d.id === ids)

    if (arr2) {
        imageContainer.src = "/assets/loading.gif"
    }
    console.log(arr2[0])
    
    itemImage = baseUrl+arr2[0].image
   
    setTimeout(function(){
        imageContainer.src = itemImage
    },200)


    itemTitle = arr2[0].title
    itemDescription = arr2[0].description
    itemPrice = arr2[0].price
    stockAvailable = arr2[0].stock

    const itemSpecs = arr2[0].specs


    itemTitleContainer.innerText = itemDescription
    itemPriceContainer.innerText = itemPrice + ' Nok'
    itemNameContainer.innerHTML = itemTitle
    stockAvailableContainer.innerHTML = "stock : "+stockAvailable
    specsContainer.innerHTML = itemSpecs
    
}



function checkImageAvailability(image){
    let isLoaded = image.complete && image.naturalHeight !== 0
    console.log(isLoaded)
}

function failedLoad(){
    imageContainer.src = "https://dummyimage.com/450x300/dee2e6/6c757d.jpg";
    // console.log("failed to load");
    return 0
}

function succesLoad(){
    imageContainer.src = itemImage
    console.log("failed to load")
}


// function to see if you can loan

const grantedForLoan = () => { 
    maxLoan = bankBalance*2;
    let text = "You are eligible for loan. Your current Balance is " + bankBalance + " kr.\nYou are allowed to loan for a max of: "+ maxLoan +"\nWould you like to proceed?"

if (confirm(text) == true) {
    let amount = 0;
        
        amount = prompt('Input an amount you wish to loan:', maxLoan);
        // console.log(amount);
        if (amount != null) { 

            if (amount <= maxLoan) {
                loanAmount = parseFloat(amount);
          
                alert('You loaned '+ loanAmount+'kr.');
                bankBalance  =  (loanAmount + parseFloat(bankBalance) ); 
                displayCurrentLoanStatus(loanAmount.toFixed(2));
                
                displayCurrentBankBalance(bankBalance);
                loanStatusDiv.style.display = "block";
                payBtn.style.display = "block";

            }else{
                alert('Cannot continue, desired amount exceeds the allowable amount.');   
            }
        
        }else{

            alert("maybe next time.");
        }

}
else {
    console.log("canceled")
}
}


// Alert if you have an existing loan

const alertForLoan = () => {
    getLoanBtn.addEventListener('click', function(e) {

        if (loanAmount <= 0) { 
           
            if (bankBalance > 0){
        
                grantedForLoan();
        
            }else {
            alert ("Your bank balance is not granting you to loan.")
            }

        }else{
            alert("You still have an outstanding loan. Cannot proceed...")
        }

    }, false)

}

alertForLoan()




let bankBalance = 0
let incomePerWork = 100 
let currentIncome = 0  
let loanAmount = 0 


// Displaying the value of currentIncome, bankBalance and Loanstatus

const displayCurrentIncome = (currentIncome) => {
    totalWorkAmount.innerText =  currentIncome.toFixed(2)
}

const displayCurrentBankBalance = (bankBalance) => {
    currentBalanceBank.innerText = bankBalance.toFixed(2)
}

const displayCurrentLoanStatus = (loanAmount) => {
    
    if (loanAmount <= 0) {
        loanStatusDiv.style.display = "none";
        payBtn.style.display = "none";
    }else{
        loanAmountContainer.innerHTML = 'Oustanding loan: <strong>'+loanAmount+'</strong>';
    }
  
}


// Work handler

const workWorkWork = () => {
    currentIncome += incomePerWork
    console.log("current Income: "+currentIncome)
    displayCurrentIncome(currentIncome) 
}   

workBtn.addEventListener('click', workWorkWork)


// Deposit current income to bank if there´s an existing loan
// 10% must be paid to loan and the rest will be transfered to bank balance

const ifTheresLoanHowToDeposit = () =>{
 
    let tenPercentPayment = currentIncome * .10
    
    if (tenPercentPayment > loanAmount) {  
        toPayAmount = currentIncome - loanAmount
        loanAmount = 0
        displayCurrentLoanStatus(loanAmount)  

        bankBalance += toPayAmount-loanAmount  
        displayCurrentBankBalance(bankBalance)  

        currentIncome = 0  
        displayCurrentIncome(currentIncome) 

    }else if (tenPercentPayment == loanAmount) {  
        loanAmount = 0
        displayCurrentLoanStatus(loanAmount) 

        bankBalance += currentIncome - tenPercentPayment
        displayCurrentBankBalance(bankBalance) 

        currentIncome = 0  
        displayCurrentIncome(currentIncome)  

    }else{  
        loanAmount -= tenPercentPayment
        displayCurrentLoanStatus(loanAmount) 

        bankBalance += currentIncome - tenPercentPayment
        displayCurrentBankBalance(bankBalance)  

        currentIncome = 0  
        displayCurrentIncome(currentIncome)  
    }
       
    
}


// Deposit Handler

const depositToBank = () => {

        // before deposit make sure to have current income

        if (currentIncome != 0) { 

            if (loanAmount <=0) { // if you dont have loan
                bankBalance += currentIncome
                console.log("bank  balance: "+bankBalance)
                displayCurrentBankBalance(bankBalance)  

                currentIncome = 0 // reset to zero if already deposited
                displayCurrentIncome(currentIncome) 

            }else{ // if you have loan call this function
                ifTheresLoanHowToDeposit() 
            }
       
        }else{
            alert("Nothing to deposit...")
        }
}

transferToBank.addEventListener('click', depositToBank)  


// repayBtn Function

payBtn.addEventListener('click', function(){

    // check if you have work pay  

    if (currentIncome >= 1) {
        let amountTopay = prompt("Amount to pay ", currentIncome);

        if (amountTopay != null) {  // check if click ok or cancel
        
            if (amountTopay > currentIncome) {

                alert("cannot be");
                
            }else if(amountTopay < currentIncome){

                alert("cannot be");

            }else{

                if (amountTopay >= loanAmount) {
                    
                    let excessMoney = amountTopay-loanAmount;
                    currentIncome = excessMoney;
                    displayCurrentIncome(currentIncome);     
                
                    loanAmount = 0;
                    displayCurrentLoanStatus(loanAmount);

                }else{
                    loanAmount -= amountTopay;
                    currentIncome = 0;
                    displayCurrentIncome(currentIncome);  
                    displayCurrentLoanStatus(loanAmount);
                }

                // alert("ok");

            }

        }
        

    }else{
        alert("Sorry. You dont have current income.");
    }

});


// Buy hadler

const hanldePay = () => {
    if (itemPrice > 0) {

        if (bankBalance >= itemPrice) {
            const promptText = "Are you sure you want to proceed?"

            if (confirm(promptText) == true) {
                bankBalance -= itemPrice
                displayCurrentBankBalance(bankBalance)
                alert("Congratulations! You are now the ower of "+itemTitle+"!")  
                
                let itemDropDownValue = document.getElementById('sel1');

                // set time not to load at the same time

                setTimeout(function(){
                    document.getElementById('card-img-top').src = "https://dummyimage.com/450x300/dee2e6/6c757d.jpg";
                    stockAvailableContainer.innerHTML = "0"
                    specsContainer.innerHTML = 'Select item'
                    itemTitleContainer.innerText = 'Please select an item'
                    itemPriceContainer.innerText = ' Nok'
                    buyNowButton.style.display = 'none'
                }, 500);
               
                itemDropDownValue.value = 0;
            }
            else{
                console.log("Cancelled")
            }    
        }          
        else{
            alert("You do not have enough balance in your account.")
        }
    }
    else{
        alert("Please select an item.")
    }  
}

buyThisItemBtn.addEventListener('click', hanldePay)